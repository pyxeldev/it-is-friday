function itIsFriday() {
  switch (new Date().getDay()) {
    case 4:
      return 'Soon';
    case 5:
      return 'Yes';
    default:
      return 'No';
  }
}

document
  .querySelector('p')
  .innerHTML = itIsFriday();
